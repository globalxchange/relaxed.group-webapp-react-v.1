import { useContext, useEffect, useMemo, useState } from 'react';
import { AppContext } from '../../contexts/AppContext';
import ProgressiveImage from '../ProgressiveImage';
import AppPopup from './AppPopup';
import Skeleton from 'react-loading-skeleton';

const GroupCarousel = ({ className = '' }) => {
  const { appData, isMobile, allGroups } = useContext(AppContext);

  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [selectedAppData, setSelectedAppData] = useState('');

  useEffect(() => {
    if (!isMobile) {
      setIsPopupOpen(false);
      setSelectedAppData('');
    }
  }, [isMobile]);

  const openPopup = (app) => {
    if (isMobile || className === 'd-sm') {
      setSelectedAppData(app);
      setIsPopupOpen(true);
    }
  };

  const list = useMemo(() => {
    let arr = [...allGroups];
    if (appData) {
      arr = [{ ...appData, website: '#' }, ...arr];
    }

    return arr;
  }, [appData, allGroups]);

  return (
    <div className={`groups-carousel-container ${className}`}>
      {allGroups
        ? list?.map((item) => (
            <div className="group-item-container" key={item._id}>
              <div className="group-tooltip-container">
                <div className="group-name-container">
                  <img src={item.icon} alt="" className="group-icon" />
                  <div className="group-name">{item?.groupname}</div>
                </div>
                <div className="group-desc">
                  {item.description.substring(0, 100)}...
                </div>
                <a
                  href={item.website}
                  target={item.website !== '#' && '_blank'}
                  rel="noopener noreferrer"
                  className="web-link"
                >
                  Website
                </a>
              </div>
              <div
                onClick={() => openPopup(item)}
                className="group-item"
                style={{ backgroundColor: `#${item?.other_data?.colorcode}` }}
              >
                <ProgressiveImage
                  src={item?.white_icon}
                  alt=""
                  className="group-img"
                  width={50}
                  height={50}
                />
              </div>
            </div>
          ))
        : [...Array(10)].map((_, i) => (
            <Skeleton key={i} width={170} height={130} />
          ))}
      <AppPopup
        isOpen={isPopupOpen}
        onClose={() => setIsPopupOpen(false)}
        appData={selectedAppData}
      />
    </div>
  );
};

export default GroupCarousel;
