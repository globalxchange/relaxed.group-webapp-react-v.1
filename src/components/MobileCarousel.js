import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { GX_API_ENDPOINT } from '../config';

const MobileCarousel = ({ active, items }) => {
  const isPrev = (i) => {
    if (active === 0) {
      if (i === items.length - 1) return true;
    } else if (i === active - 1) return true;
    return false;
  };
  const isNext = (i) => {
    if (active === items.length - 1) {
      if (i === 0) return true;
    } else if (i === active + 1) return true;
    return false;
  };

  return (
    <div className="carousel-wrapper">
      <div className="carousel-horiz">
        {items.map((app, i) => (
          <div
            className={`page-content ${
              active === i
                ? 'active'
                : isPrev(i)
                ? 'prev'
                : isNext(i)
                ? 'next'
                : 'inactive'
            } ${app.class}`}
            key={i}
          >
            <Item item={app} level={i} />
          </div>
        ))}
      </div>
    </div>
  );
};

const Item = ({ level, item }) => {
  const [appDetails, setAppDetails] = useState('');

  useEffect(() => {
    if (item) {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
          params: { app_code: item?.app_code },
        })
        .then(({ data }) => {
          const details = data.apps ? data.apps[0] : '';
          setAppDetails(details);
        })
        .catch((error) => {
          console.log('Error getting app details', error);
        });
    }
  }, [item]);

  const className = `item level${level}`;

  return (
    <div className={className}>
      <div className="logo-container">
        <img
          src={appDetails?.app_icon || item?.app_icon}
          alt=""
          className="app-icon"
        />
        <div className="app-header">{item.app_name}</div>
      </div>
      <div className="app-desc">{appDetails?.short_description}</div>
      <div className="action-button">Get Started</div>
    </div>
  );
};

export default MobileCarousel;
