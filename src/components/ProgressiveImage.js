import { Component, Suspense } from 'react';
import { useImage } from 'react-image';
import Skeleton from 'react-loading-skeleton';

const Image = ({ imageSrc = '', className }) => {
  const { src } = useImage({
    srcList: imageSrc,
  });

  return <img src={src} alt="" className={className} />;
};

const ProgressiveImage = ({ src, className, width = 700, height = 200 }) => (
  <ErrorBoundary
    fallback={<Skeleton height={height} width={width} className={className} />}
  >
    <Suspense
      fallback={
        <Skeleton height={height} width={width} className={className} />
      }
    >
      <Image imageSrc={src} className={className} />
    </Suspense>
  </ErrorBoundary>
);

export default ProgressiveImage;

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
  }

  render() {
    const { fallback, children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      // You can render any custom fallback UI
      return fallback;
    }

    return children;
  }
}
