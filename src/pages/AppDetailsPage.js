/* eslint-disable prefer-destructuring */
import React, { useContext, useEffect, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import { GROUP_ID } from '../config';
import { AppContext } from '../contexts/AppContext';
import ViralTV from '../components/ViralTV';
import MobilePage from './MobilePage';
import PopupModalLayout from '../layout/PopupModalLayout';
import BrandSideBar from '../components/BrandSideBar';

const AppDetailsPage = ({ appData: selectedApp }) => {
  const { appData, brandList } = useContext(AppContext);

  const [isMobile, setIsMobile] = useState(false);
  const [isNoWebistePopupOpen, setIsNoWebistePopupOpen] = useState(false);

  useEffect(() => {
    screenResizeListener();
    window.addEventListener('resize', screenResizeListener);
    return () => {
      window.removeEventListener('resize', screenResizeListener);
    };
  }, []);

  const list = useMemo(() => {
    const CURRENT_DATA = {
      brand_code: GROUP_ID,
      name: appData?.groupname || 'Viral.Group',
      headerText: 'For Anyone Who Wants To Make Money',
      description:
        'A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin ownership records are stored in a ledger existing in a form of computerized database using strong cryptography to secure transaction records',
      path: '/',
      appPath: '/',
      loginPath: 'https://affiliate.app/apps/affiliate/login',
      registerPath: 'https://affiliate.app/apps/affiliate/registration',
      logo: appData?.logo || '',
      colored_icon: appData?.icon || '',
      // cover: coverOne,
      disableRoute: true,
    };

    return [CURRENT_DATA, ...(brandList || [])];
  }, [appData, brandList]);

  const colorCode = useMemo(() => {
    let color = '';

    if (selectedApp?.other_data?.primarycolourcode) {
      color = `#${selectedApp?.other_data?.primarycolourcode}`;
    } else if (selectedApp?.other_data?.secondarycolourcode) {
      color = `#${selectedApp?.other_data?.secondarycolourcode}`;
    }

    return color;
  }, [selectedApp]);

  const screenResizeListener = () => {
    setIsMobile(window.innerWidth <= 576);
  };

  if (isMobile) {
    return <MobilePage selectedApp={selectedApp} />;
  }

  // console.log('appData', selectedApp);
  // console.log('appData', appData);
  // console.log('colorCode', colorCode);
  // console.log('appDetails', appDetails);

  return (
    <div className="app-details-page-wrapper">
      <div className="app-switcher-container">
        <div className="scroll-container">
          {list.map((item) => (
            <Link
              to={item.appPath || `/brands/${item.brand_code}`}
              key={item.name}
              className={`app-switch-item ${
                selectedApp?.name === item.name ? 'active' : ''
              } ${item?.brand_code === GROUP_ID ? 'active' : ''}`}
              data-tip={item?.name}
            >
              <img src={item?.colored_icon} alt="" className="app-icon" />
            </Link>
          ))}
          <ReactTooltip
            place="right"
            type="dark"
            effect="solid"
            border
            borderColor="#343C5B"
            backgroundColor="#fff"
            textColor="#343C5B"
            className="tooltip"
          />
        </div>
      </div>
      <div className="app-details-wrapper mx-0">
        <div className="app-details-container">
          <div className="app-logo-container">
            <img src={selectedApp?.colored_icon} alt="" className="app-logo" />
            <div className="app-name" style={{ color: colorCode }}>
              {selectedApp?.name}
            </div>
          </div>
          <div className="app-desc">{selectedApp?.description}</div>
          {/* <div className="action-container">
            <div className="action-button">
              <img
                src={require('../assets/dd-coloud-icon.svg').default}
                alt=""
                className="action-icon"
              />
            </div>

            <div className="action-button">
              <img
                src={require('../assets/valuation-icon.svg').default}
                alt=""
                className="action-icon"
              />
            </div>
          </div> */}
          {selectedApp?.website ? (
            <a
              className="app-link"
              href={selectedApp?.website}
              target="_blank"
              rel="noreferrer"
            >
              <span>Click Here</span> To Visit{' '}
              <img
                src={selectedApp?.other_data?.coloredfulllogo}
                alt=""
                className="app-icon"
              />
            </a>
          ) : (
            <div
              className="app-link"
              onClick={() => setIsNoWebistePopupOpen(true)}
            >
              <span>Click Here</span> To Visit {selectedApp?.name}
            </div>
          )}
        </div>
        <BrandSideBar selectedApp={selectedApp} colorCode={colorCode} />
        {/* <ViralTV colorCode={colorCode} selectedApp={selectedApp} /> */}
      </div>
      <PopupModalLayout
        isOpen={isNoWebistePopupOpen}
        onClose={() => setIsNoWebistePopupOpen(false)}
        noHeader
        width={700}
      >
        <div className="no-web-popup">
          <img
            src={selectedApp?.other_data?.coloredfulllogo}
            alt=""
            className="app-logo"
          />
          <div className="bottom-bar" style={{ backgroundColor: colorCode }}>
            The Website For {selectedApp?.name} Will Be Up Soon
          </div>
        </div>
      </PopupModalLayout>
    </div>
  );
};

export default AppDetailsPage;
