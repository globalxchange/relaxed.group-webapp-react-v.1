import axios from 'axios';
import React, { createContext, useEffect, useState } from 'react';
import { GROUP_ID, GX_API_ENDPOINT } from '../config';

export const AppContext = createContext();

const AppContextProvider = ({ children }) => {
  const [appData, setAppData] = useState('');
  const [isMobile, setIsMobile] = useState(false);
  const [brandList, setBrandList] = useState([]);
  const [allGroups, setAllGroups] = useState([]);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/gxb/app/gxlive/operator/app/groups/get`, {
        params: { group_id: GROUP_ID },
      })
      .then(({ data }) => {
        console.log('AppData', data);

        const group = data.groups ? data.groups[0] : '';
        setAppData(group);
      })
      .catch((error) => {
        console.log('Error getting app data', error);
      });

    axios
      .get(`${GX_API_ENDPOINT}/gxb/app/gxlive/operator/brands/get`, {
        params: { parent_group_id: GROUP_ID },
      })
      .then(({ data }) => {
        console.log('Brands List', data);

        const brands = data.brands || [];
        setBrandList(brands);
      })
      .catch((error) => {
        console.log('Error getting Brands Data', error);
      });

    axios
      .get(`${GX_API_ENDPOINT}/gxb/app/gxlive/operator/app/groups/get`)
      .then(({ data }) => {
        const list = data.groups || [];

        const filtredList = list.filter((x) => x.group_id !== GROUP_ID);

        setAllGroups(filtredList);
      })
      .catch((error) => {});

    screenResizeListener();
    window.addEventListener('resize', screenResizeListener);
    return () => {
      window.removeEventListener('resize', screenResizeListener);
    };
  }, []);

  const screenResizeListener = () => {
    setIsMobile(window.innerWidth <= 576);
  };

  return (
    <AppContext.Provider value={{ appData, isMobile, brandList, allGroups }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
